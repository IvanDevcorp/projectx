<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge" >
<meta name="viewport" content="width=device-width, initial-scale=2">
<meta name="description" content=" SEO">
<meta name="author" content="Abdullin A.O.">
<link rel="icon" href="<?php echo get_file_uri("assets/images/favicon.png"); ?>" />

<title><?php echo get_setting('app_title'); ?></title>