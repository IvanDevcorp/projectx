<div class="tab-content">
    <?php
    $url = get_uri("team_members/save_social_links/" . $user_id);
    if (isset($user_type) && $user_type === "client") {
        $url = get_uri("clients/save_contact_social_links/" . $user_id);
    };
    echo form_open($url, array("id" => "social-links-form", "class" => "general-form dashed-row white", "role" => "form"));
    ?>
    <div class="panel">
        <div class="panel-default panel-heading">
            <h4> <?php echo lang('social_links'); ?></h4>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label for="facebook" class=" col-md-2">Facebook</label>
                <div class=" col-md-10">
                    <?php
                    echo form_input(array(
                        "id" => "facebook",
                        "name" => "facebook",
                        "value" => $model_info->facebook,
                        "class" => "form-control",
                        "placeholder" => "https://www.facebook.com/"
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label for="twitter" class=" col-md-2">Twitter</label>
                <div class=" col-md-10">
                    <?php
                    echo form_input(array(
                        "id" => "twitter",
                        "name" => "twitter",
                        "value" => $model_info->twitter,
                        "class" => "form-control",
                        "placeholder" => "https://twitter.com/"
                    ));
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label for="instagram" class=" col-md-2">Instagram</label>
                <div class=" col-md-10">
                    <?php
                    echo form_input(array(
                        "id" => "instagram",
                        "name" => "instagram",
                        "value" => $model_info->instagram,
                        "class" => "form-control",
                        "placeholder" => "https://instagram.com/"
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label for="vk" class=" col-md-2">vk</label>
                <div class=" col-md-10">
                    <?php
                    echo form_input(array(
                        "id" => "vk",
                        "name" => "vk",
                        "value" => $model_info->vk,
                        "class" => "form-control",
                        "placeholder" => "https:/vk.com/"
                    ));
                    ?>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-primary"><span class="fa fa-check-circle"></span> <?php echo lang('save'); ?></button>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("#social-links-form").appForm({
            isModal: false,
            onSuccess: function(result) {
                appAlert.success(result.message, {duration: 10000});
            }
        });
    });
</script>    